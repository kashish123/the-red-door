package com.example.hp.thereddoor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by HP on 11/4/2015.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.Holder> {
    ArrayList<AppointmentDetails> data;
    Context context;
    int rowLayout;
    CommonUtils.OnItemClickListener onItemClickListener;


    public RecyclerViewAdapter(ArrayList<AppointmentDetails> data, Context context, int rowLayout) {
        this.data = data;
        this.context = context;
        this.rowLayout = rowLayout;
    }

    @Override
    public RecyclerViewAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(rowLayout, null);
        Holder viewHolder = new Holder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(RecyclerViewAdapter.Holder holder, final int position) {
        if (Appointments.value == false) {
            holder.customerName.setText(data.get(position).getCustomerName());
            holder.service.setText(data.get(position).getService());
            holder.location.setText(data.get(position).getLocation());
            holder.duration.setText(data.get(position).getDuration());
            holder.timeToAccept.setText("");
            holder.dateAndTimeOfService.setText(data.get(position).getTimeOfService() + "," + data.get(position).getDateOfService());

            holder.appointmentListContainer.setOnClickListener(null);
        } else {
            holder.customerName.setText(data.get(position).getCustomerName());
            holder.service.setText(data.get(position).getService());
            holder.location.setText(data.get(position).getLocation());
            holder.duration.setText(data.get(position).getDuration());
            holder.timeToAccept.setText(data.get(position).getTimeToAccept());
            holder.dateAndTimeOfService.setText(data.get(position).getTimeOfService() + "," + data.get(position).getDateOfService());
            holder.appointmentListContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
onItemClickListener.onItemClick(position);
                }
            });

        }
    }


    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView customerName, service, location, duration, dateAndTimeOfService, timeToAccept;
    //    ImageButton locationOnMap;
        LinearLayout appointmentListContainer;

        public Holder(View view) {
            super(view);
            timeToAccept = (TextView) view.findViewById(R.id.tv_time_left);
            appointmentListContainer = (LinearLayout) view.findViewById(R.id.linearTop);
            customerName = (TextView) view.findViewById(R.id.edittext);
            service = (TextView) view.findViewById(R.id.massage);
            location = (TextView) view.findViewById(R.id.location);
            duration = (TextView) view.findViewById(R.id.time);
            dateAndTimeOfService = (TextView) view.findViewById(R.id.tv_time_date);
            //   locationOnMap = (ImageButton)view.findViewById(R.id.locationOnMap);

        }
    }

    public void setOnItemClickListener(CommonUtils.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

}
















































































































