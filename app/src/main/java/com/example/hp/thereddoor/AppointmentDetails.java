package com.example.hp.thereddoor;

/**
 * Created by HP on 11/2/2015.
 */
public class AppointmentDetails {

    String customerName,service,location,dateOfService,TimeOfService,duration,timeToAccept,customerNotes;

    public String getTimeToAccept() {
        return timeToAccept;
    }

    public void setTimeToAccept(String timeToAccept) {
        this.timeToAccept = timeToAccept;
    }

    public String getCustomerNotes() {
        return customerNotes;
    }

    public void setCustomerNotes(String customerNotes) {
        this.customerNotes = customerNotes;
    }

    public String getLocation() {
        return location;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getDateOfService() {
        return dateOfService;
    }

    public void setDateOfService(String dateOfService) {
        this.dateOfService = dateOfService;
    }

    public String getTimeOfService() {
        return TimeOfService;
    }

    public void setTimeOfService(String timeOfService) {
        TimeOfService = timeOfService;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}

