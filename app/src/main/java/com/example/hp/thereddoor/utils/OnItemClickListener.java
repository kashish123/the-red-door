package com.example.hp.thereddoor.utils;

/**
 * Created by HP on 11/5/2015.
 */
public interface OnItemClickListener {
    void onItemClick(int position);
}
