package com.example.hp.thereddoor;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;

public class Appointments extends Activity implements View.OnClickListener,CommonUtils.OnItemClickListener, AdapterView.OnItemClickListener {


    //    TextView timeToAccept;
    Button scheduled, invitations;
    RecyclerView appointments;
    AppointmentDetails scheduledAppointments;
    ArrayList<AppointmentDetails> appointmentsArrayList = new ArrayList<>();
    RecyclerViewAdapter recylcerViewAdapter;
    public static Boolean value = false;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    ArrayList<NavDrawerItems> navDrawerItems= new ArrayList<NavDrawerItems>();;
    NavDrawerAdapter navDrawerAdapter;
    ListView drawerList;
    ImageButton back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_appointments);


        scheduled = (Button) findViewById(R.id.scheduled);
        invitations = (Button) findViewById(R.id.invitations);
        drawerList = (ListView) findViewById(R.id.left_drawer);

        scheduled.setOnClickListener(this);
        invitations.setOnClickListener(this);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);


        back = (ImageButton) findViewById(R.id.imageButton);
        back.setOnClickListener(this);


        appointments = (RecyclerView) findViewById(R.id.recyclerView);
        recylcerViewAdapter = new RecyclerViewAdapter(appointmentsArrayList, this, R.layout.appointment);
        appointments.setLayoutManager(new LinearLayoutManager(this));
        appointments.setAdapter(recylcerViewAdapter);
        for (int i = 0; i < 3; i++) {
            scheduledAppointments = new AppointmentDetails();

            scheduledAppointments.setCustomerName(String.valueOf(i));
            scheduledAppointments.setDateOfService("31,Oct,2015");
            scheduledAppointments.setTimeOfService("2:15 PM");
            scheduledAppointments.setDuration("60 min");
            scheduledAppointments.setLocation("Sector 19, Chandigarh");
            scheduledAppointments.setService("chabd");
            scheduledAppointments.setTimeToAccept("Time to accept 5:00");
            appointmentsArrayList.add(scheduledAppointments);
            recylcerViewAdapter.notifyDataSetChanged();
        }


        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }
        };


        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.setDrawerListener(drawerToggle);
      //  navDrawerItems = new ArrayList<NavDrawerItems>();
        navDrawerItems.add(new NavDrawerItems("LOGOUT"));
        navDrawerAdapter = new NavDrawerAdapter(getApplicationContext(), R.layout.navigation_drawer_layout, navDrawerItems);
        drawerList.setAdapter(navDrawerAdapter);
        navDrawerAdapter.notifyDataSetChanged();
        drawerList.setOnItemClickListener(this);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action bar actions click
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        drawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(this, InvitationDetail.class);
        startActivity(intent);
    }


    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.scheduled:
                scheduled.setBackgroundResource(R.mipmap.small_btn_pressed);
                invitations.setBackgroundResource(R.mipmap.small_btn_normal);
                scheduled.setTextColor(Color.WHITE);
                invitations.setTextColor(Color.BLACK);
                value = false;
                recylcerViewAdapter.notifyDataSetChanged();
                break;


            case R.id.invitations:
                invitations.setBackgroundResource(R.mipmap.small_btn_pressed);
                scheduled.setBackgroundResource(R.mipmap.small_btn_normal);
                scheduled.setTextColor(Color.BLACK);
                invitations.setTextColor(Color.WHITE);
                value = true;
                recylcerViewAdapter.notifyDataSetChanged();
                break;


            case R.id.imageButton:
                drawerLayout.openDrawer(drawerList);
                break;


        }

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                Intent signInIntent = new Intent(this, signIn.class);
                startActivity(signInIntent);
                finish();
        }
    }
}

































