package com.example.hp.thereddoor;

/**
 * Created by HP on 11/3/2015.
 */
public class NavDrawerItems {



    private String title;


    public NavDrawerItems(String title) {
        this.title = title;

    }

    public String getTitle() {
        return this.title;
    }


    public void setTitle(String title) {
        this.title = title;
    }
}
