package com.example.hp.thereddoor;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by HP on 11/3/2015.
 */
public class NavDrawerAdapter extends ArrayAdapter<String> {
    private Context context;
    private ArrayList<NavDrawerItems> navDrawerItems;
    private int rowLayout;

    public NavDrawerAdapter(Context context,int layoutResourseId, ArrayList<NavDrawerItems> navDrawerItems){
        super(context,layoutResourseId);
        this.context = context;
        this.navDrawerItems = navDrawerItems;
        this.rowLayout = layoutResourseId;
    }


    @Override
    public int getCount() {
        return navDrawerItems.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = View.inflate(context, rowLayout ,null);

        TextView textView = (TextView) convertView.findViewById(R.id.title);
        textView.setText(navDrawerItems.get(position).getTitle());
        return convertView;
    }
}
