package com.example.hp.thereddoor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class Aboutme extends Activity implements View.OnClickListener {
    ImageButton Back;
    Button savetext;
    Intent in;
    EditText characterCount;
    TextView number_of_character;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_aboutme);

        characterCount = (EditText) findViewById(R.id.about);
        number_of_character = (TextView) findViewById(R.id.count);
        characterCount.addTextChangedListener(mTextEditorWatcher);


        savetext = (Button) findViewById(R.id.save);
        savetext.setOnClickListener(this);
        Back = (ImageButton) findViewById(R.id.backbutton2);
        Back.setOnClickListener(this);
    }


    private final TextWatcher mTextEditorWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            number_of_character.setText(String.valueOf(s.length())+"|"+"140");
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_aboutme, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {


        switch (v.getId())
        {

            case R.id.save:
              in=new Intent(this,Address.class);
                startActivity(in);
                break;

            case R.id.backbutton2:
                in=new Intent(this,signIn.class);
                startActivity(in);
                break;
        }


    }
}
