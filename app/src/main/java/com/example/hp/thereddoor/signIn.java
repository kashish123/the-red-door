package com.example.hp.thereddoor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class signIn extends Activity implements View.OnClickListener {
    EditText Enteremail, Password;
    Button sign;
    ImageButton Back;
    Intent in;
    TextView textView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_sign_in);

        textView = (TextView) findViewById(R.id.regis);
        String text = "<font color=#ABACAD>Not Registered Yet? </font>" +
                " <font color=#CD2E44><u>SIGN UP</u></font>" +
                " <font color=#ABACAD>here</font>";
        textView.setText(Html.fromHtml(text));
        textView.setOnClickListener(this);

        sign = (Button) findViewById(R.id.signinbutton);
        sign.setOnClickListener(this);


   /*     findViewById(R.id.signinbutton).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {


                
                    final String email = Enteremail.getText().toString();
                    if (!isValidEmail(email)) {
                        Enteremail.setError("Invalid Email");
                    }

                    final String pass = Password.getText().toString();
                    if (!isValidPassword(pass)) {
                        Password.setError("Invalid Password");
                    }
                }




        });      */


        Enteremail = (EditText) findViewById(R.id.enteremail);
        Password = (EditText) findViewById(R.id.enterpass);







        Back=(ImageButton)findViewById(R.id.backbutton);

        Back.setOnClickListener(this);
    }

    // validating email id
  /*  private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // validating password with retype password
    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() > 6) {
            return true;
        }
        return false;
    }    */







    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_in, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.backbutton:
                in=new Intent(this,MainActivity.class);
                startActivity(in);
                break;

          case R.id.signinbutton:

                in=new Intent(this,Appointments.class);
                startActivity(in);
                break;

            case R.id.regis:
                in=new Intent(this,SignUp.class);
                startActivity(in);
                break;


        }
    }
}
