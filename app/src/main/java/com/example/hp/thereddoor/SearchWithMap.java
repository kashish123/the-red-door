package com.example.hp.thereddoor;

import android.app.Dialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.hp.thereddoor.adapters.SelectLocationAdapter;
import com.example.hp.thereddoor.utils.ConvertLocationToLatLong;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class SearchWithMap extends FragmentActivity implements View.OnClickListener, AdapterView.OnItemClickListener, OnMapReadyCallback, GoogleMap.OnMyLocationChangeListener {
    Button don;
    EditText chooseLocation;
    ImageButton back;
    boolean locationSelected=false;
    double[] latlng;
    AutoCompleteTextView autoCompView;

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_search_with_map);
        don = (Button) findViewById(R.id.button2);
        don.setOnClickListener(this);

        back = (ImageButton) findViewById(R.id.backbutton2);
        back.setOnClickListener(this);
        chooseLocation = (EditText) findViewById(R.id.chooselocation);

         autoCompView = (AutoCompleteTextView) findViewById(R.id.chooselocation);
        autoCompView.setAdapter(new SelectLocationAdapter(this, R.layout.auto_complete_text_view));
        autoCompView.setOnItemClickListener(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        latlng = new double[2];
        mMap = mapFragment.getMap();
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
        if (status != ConnectionResult.SUCCESS) {
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        } else {
            SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mMap = fm.getMap();
            mMap.setMyLocationEnabled(true);
            mMap.setOnMyLocationChangeListener(this);
        }
    }


    private void setLocation() {
        mMap.clear();
        LatLng latLng = new LatLng(latlng[0], latlng[1]);
        mMap.addMarker(new MarkerOptions().position(latLng));
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.setMyLocationEnabled(true);
    }

    public boolean googleMapAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status)
            return true;
        else
            return false;
    }


    private class LatLngHandler extends android.os.Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Bundle bundle = msg.getData();
                    latlng = bundle.getDoubleArray("LatLng");
                    locationSelected = true;
                    setLocation();
                    break;
                default:
                    Toast.makeText(SearchWithMap.this, "Unable to find address", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
    }



    @Override
    public void onClick(View v) {


        switch (v.getId()) {

            case R.id.button2:

                break;


            case R.id.backbutton2:
                Intent in = new Intent(this, Address.class);
                startActivity(in);
                break;
        }

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

        String agentAddress = (String) adapterView.getItemAtPosition(position);
        if (!googleMapAvailable()) {
            Toast.makeText(SearchWithMap.this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();

        } else {

            ConvertLocationToLatLong convertLocationToLatLng = new ConvertLocationToLatLong();
            convertLocationToLatLng.getLatLng(agentAddress, getApplicationContext(), new LatLngHandler());
        }
        Toast.makeText(this, agentAddress, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onMyLocationChange(Location location) {
        if (!locationSelected) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
            mMap.addMarker(new MarkerOptions().position(latLng));
        }

    }
}

