package com.example.hp.thereddoor.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by HP on 11/6/2015.
 */
public class ConvertLocationToLatLong {
    public static void getLatLng(final String customerLocation, final Context context, final Handler handler)
    {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                double[] latLng= new double[2];
                try {
                    List customerAddress = geocoder.getFromLocationName(customerLocation, 1);
                    if (customerAddress != null && customerAddress.size() > 0) {
                        Address address = (Address) customerAddress.get(0);
                        latLng[0]=address.getLatitude();
                        latLng[1]=address.getLongitude();

                    }
                } catch (IOException e) {
                    Log.e("Exception", "Unable to connect to Geocoder", e);
                } finally {
                    Message message = Message.obtain();
                    message.setTarget(handler);
                    message.what=1;
                    Bundle bundle = new Bundle();
                    bundle.putDoubleArray("LatLng",latLng);
                    message.setData(bundle);
                    message.sendToTarget();
                }
            }
        };
        thread.start();
    }
}
