package com.example.hp.thereddoor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SignUp extends Activity implements View.OnClickListener {
    Spinner Gender;
    ArrayAdapter adapter;
    Intent in;
    Button signup;
    ImageButton back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_sign__up);

        TextView textView=(TextView)findViewById(R.id.allreadymember);
        String text= "<font color=#ABACAD>Already a Member? </font>"+
                " <font color=#CD2E44><u>SIGN IN</u></font>";

        textView.setText(Html.fromHtml(text));
        textView.setOnClickListener(this);


        back=(ImageButton)findViewById(R.id.backbutton1);
        back.setOnClickListener(this);


        signup=(Button)findViewById(R.id.signupbutton);
        signup.setOnClickListener(this);
     /*   signup.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    signup.setBackgroundResource(R.mipmap.big_btn_pressed);

                }
                return false;
            }
        });    */






        TextView textView1=(TextView)findViewById(R.id.youagreeto);
        String text1= "<font color=#ABACAD>you agree to </font>"+
                " <font color=#CD2E44><u>Terms of Service and Private</u></font>";

        textView1.setText(Html.fromHtml(text1));
        textView1.setOnClickListener(this);



        Gender=(Spinner)findViewById(R.id.gender);
        adapter=ArrayAdapter.createFromResource(this,R.array.Gender,android.R.layout.simple_spinner_item);
        Gender.setAdapter(adapter);

        Gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                TextView gen = (TextView) view;


                Toast.makeText(getBaseContext(), gen.getText(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign__up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

       switch (v.getId())
       {

           case R.id.allreadymember:
               in=new Intent(this,signIn.class);
               startActivity(in);
               break;




           case R.id.youagreeto:


               break;


           case R.id.signupbutton:
               in=new Intent(this,Aboutme.class);
               startActivity(in);
               break;


           case R.id.backbutton1:
               in=new Intent(this,signIn.class);
               startActivity(in);

       }
    }
}
