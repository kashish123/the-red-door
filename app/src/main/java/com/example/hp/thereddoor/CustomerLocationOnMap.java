package com.example.hp.thereddoor;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.hp.thereddoor.utils.ConvertLocationToLatLong;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class CustomerLocationOnMap extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {


ImageButton back;
    double[] latlng = new double[2];
    GoogleMap googleMap;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_location_on_map);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        googleMap=mapFragment.getMap();

        back = (ImageButton)findViewById(R.id.backbutton2);
        back.setOnClickListener(this);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

        } else {
            // Show rationale and request permission.
        }
        if(!googleMapAvailable())
        {
            Toast.makeText(this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();

        }

        else
        {
            String customerLocation = getIntent().getStringExtra("location");
            ConvertLocationToLatLong convertLocationToLatLng = new ConvertLocationToLatLong();
            convertLocationToLatLng.getLatLng(customerLocation, getApplicationContext(), new LatLngHandler());
        }
    }


    @Override
    public void onMapReady(GoogleMap map) {

    }
    private void setLocation() {
        googleMap.setMyLocationEnabled(true);
        LatLng latLng = new LatLng(latlng[0], latlng[1]);
        googleMap.addMarker(new MarkerOptions().position(latLng));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
    }
    @Override
    public void onClick(View v) {
        super.onBackPressed();
    }



    public boolean googleMapAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if(ConnectionResult.SUCCESS == status)
            return true;
        else
            return false;
    }

    private class LatLngHandler extends android.os.Handler{
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Bundle bundle = msg.getData();
                    latlng = bundle.getDoubleArray("LatLng");
                    setLocation();
                    break;
                default:
                    Toast.makeText(CustomerLocationOnMap.this, "Unable to find address", Toast.LENGTH_SHORT).show();
            }
        }
    }












    }

