package com.example.hp.thereddoor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;

public class Address extends Activity implements View.OnClickListener {
    ImageButton back;
    Button save1,searchmap;
    Intent in;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_address);




searchmap=(Button)findViewById(R.id.searchonmap);
        searchmap.setOnClickListener(this);

        save1=(Button)findViewById(R.id.saveaddress);
        save1.setOnClickListener(this);
        back=(ImageButton)findViewById(R.id.backbutton3);
        back.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_address, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {

          //  case R.id.searchonmap:
            //    in=new Intent(this,MapsActivityUsingFragment.class);
             //   startActivity(in);
             //   break;


            case R.id.saveaddress:
                in=new Intent(this,Appointments.class);
                startActivity(in);
                break;



            case R.id.backbutton3:
                in=new Intent(this,Aboutme.class);
                startActivity(in);
                break;



        }
    }
}
